#pragma once

#include "session.hpp"
#include "entry.hpp"

#include <RDiamondApi.h>

#include <vector>
#include <fstream>
#include <unordered_map>
#include <boost/cstdint.hpp>
#include <boost/thread/condition_variable.hpp>

namespace rithmic
{

struct Session::Impl : RApi::AdmCallbacks, RApi::RCallbacks {
    typedef std::vector< std::string > Strings;
//    using Route = std::vector< std::string >;
//    using Order = std::tuple< std::string, Session::Uint, double, Session::Uchar, Session::Uchar, Session::Uchar >;
//    using RouteMap = std::unordered_map< std::string, Route >;

    std::string username;
    std::string password;
    bool debug;
    Strings enviroment;
    std::string logPath;
    std::ofstream log;

    RApi::REngine * engine;
    Session * session;

    std::unordered_map< std::string, Entry > entries;
    boost::mutex mutex;
    boost::condition_variable onReady;
    bool isOpen;
    bool isLogged;

    struct {
        RApi::AccountInfo info;
        std::string fcmId;
        std::string ibId;
        std::string id;
    } account;

    struct Tool {
        std::string symbol[ 2 ];

        tsNCharcb exchange;
        tsNCharcb ticker;

        enum { Exchange, Ticker };

        bool operator !() const;

        static std::string toString( const Tool & tool );
        static std::string toString( const tsNCharcb & exchange, const tsNCharcb & ticker );
        static Tool fromString( const std::string & string );
        static bool isValid( const Tool & tool );
    };

    enum { InvalidConnection = -1 };
    enum { TypeBuy, TypeSell };
    enum { QuoteBid, QuoteAsk, BestBid, BestAsk };
    enum { OrderFailed = 1, OrderRejected, OrderFilled, OrderConfirmed, OrderBursted };
    enum { StateLogged = 1, StateReady = 2, StateQuote = 4 };
    enum { ConnectionOpened, ConnectionReady = ConnectionOpened, ConnectionBroken = 1001, ConnectionClosed };

    static std::string getUser();
    static std::string getCwd();
    static tsNCharcb toNCharcb( const char * s );
    static tsNCharcb toNCharcb( const std::string & s );
    static std::string fromNCharcb( const tsNCharcb & s );
    static bool isNCharcbEqual( const tsNCharcb & s1, const tsNCharcb & s2 );
    static boost::uint64_t toNanoTime( int seconds, int nanoseconds );
    static boost::uint64_t toMicroTime( int seconds, int microseconds );

    Tool makeTool( const std::string & symbol );

    int Alert( RApi::AlertInfo * info, void * context, int * code ) override;

    int AccountList( RApi::AccountListInfo * info, void * context, int * code ) override;
    int AskQuote( RApi::AskInfo * info, void * context, int * code ) override;
    int BestAskQuote( RApi::AskInfo * info, void * context, int * code ) override;
    int BestBidAskQuote( RApi::BidInfo * infoBid, RApi::AskInfo * infoAsk, void * context, int * code ) override;
    int BestBidQuote( RApi::BidInfo * info, void * context, int * code ) override;
    int BidQuote( RApi::BidInfo * info, void * context, int * code ) override;
    int BinaryContractList( RApi::BinaryContractListInfo * info, void * context, int * code ) override;
    int BustReport( RApi::OrderBustReport * report, void * context, int * code ) override;
    int CancelReport( RApi::OrderCancelReport * report, void * context, int * code ) override;
    int ClosePrice( RApi::ClosePriceInfo * info, void * context, int * code ) override;
    int ClosingIndicator( RApi::ClosingIndicatorInfo * info, void * context, int * code ) override;
    int EndQuote( RApi::EndQuoteInfo * info, void * context, int * code ) override;
    int EquityOptionStrategyList( RApi::EquityOptionStrategyListInfo * info, void * context, int * code ) override;
    int ExchangeList( RApi::ExchangeListInfo * info, void * context, int * code ) override;
    int ExecutionReplay( RApi::ExecutionReplayInfo * info, void * context, int * code ) override;
    int FailureReport( RApi::OrderFailureReport * report, void * context, int * code ) override;
    int FillReport( RApi::OrderFillReport * report, void * context, int * code ) override;
    int HighPrice( RApi::HighPriceInfo * info, void * context, int * code ) override;
    int InstrumentByUnderlying( RApi::InstrumentByUnderlyingInfo * info, void * context, int * code ) override;
    int LimitOrderBook( RApi::LimitOrderBookInfo * info, void * context, int * code ) override;
    int LineUpdate( RApi::LineInfo * info, void * context, int * code ) override;
    int LowPrice( RApi::LowPriceInfo * info, void * context, int * code ) override;
    int MarketMode( RApi::MarketModeInfo * info, void * context, int * code ) override;
    int ModifyReport( RApi::OrderModifyReport * report, void * context, int * code ) override;
    int NotCancelledReport( RApi::OrderNotCancelledReport * report, void * context, int * code ) override;
    int NotModifiedReport( RApi::OrderNotModifiedReport * report, void * context, int * code ) override;
    int OpenInterest( RApi::OpenInterestInfo * info, void * context, int * code ) override;
    int OpenOrderReplay( RApi::OrderReplayInfo * info, void * context, int * code ) override;
    int OpenPrice( RApi::OpenPriceInfo * info, void * context, int * code ) override;
    int OpeningIndicator( RApi::OpeningIndicatorInfo * info, void * context, int * code ) override;
    int OptionList( RApi::OptionListInfo * info, void * context, int * code ) override;
    int OrderReplay( RApi::OrderReplayInfo * info, void * context, int * code ) override;
    int OtherReport( RApi::OrderReport * report, void * context, int * code ) override;
    int PasswordChange( RApi::PasswordChangeInfo * info, void * context, int * code ) override;
    int PnlReplay( RApi::PnlReplayInfo * info, void * context, int * code ) override;
    int PnlUpdate( RApi::PnlInfo * info, void * context, int * code ) override;
    int PriceIncrUpdate( RApi::PriceIncrInfo * info, void * context, int * code ) override;
    int ProductRmsList( RApi::ProductRmsListInfo * info, void * context, int * code ) override;
    int RefData( RApi::RefDataInfo * info, void * context, int * code ) override;
    int RejectReport( RApi::OrderRejectReport * report, void * context, int * code ) override;
    int SettlementPrice( RApi::SettlementPriceInfo * info, void * context, int * code ) override;
    int SingleOrderReplay( RApi::SingleOrderReplayInfo * info, void * context, int * code ) override;
    int SodUpdate( RApi::SodReport * report, void * context, int * code ) override;
    int StatusReport( RApi::OrderStatusReport * report, void * context, int * code ) override;
    int Strategy( RApi::StrategyInfo * info, void * context, int * code ) override;
    int StrategyList( RApi::StrategyListInfo * info, void * context, int * code ) override;
    int TradeReplay( RApi::TradeReplayInfo * info, void * context, int * code ) override;
    int TradeRoute( RApi::TradeRouteInfo * info, void * context, int * code ) override;
    int TradeRouteList( RApi::TradeRouteListInfo * info, void * context, int * code ) override;
    int TriggerPulledReport( RApi::OrderTriggerPulledReport * report, void * context, int * code ) override;
    int TriggerReport( RApi::OrderTriggerReport * report, void * context, int * code ) override;
    int TradeCondition( RApi::TradeInfo * info, void * context, int * code ) override;
    int TradeCorrectReport( RApi::OrderTradeCorrectReport * report, void * context, int * code ) override;
    int TradePrint( RApi::TradeInfo * info, void * context, int * code ) override;
    int TradeVolume( RApi::TradeVolumeInfo * info, void * context, int * code ) override;
};


} // namespace rithmic

#include "rithmic.h"
#include "session.hpp"
#include "session_test.hpp"
#include <boost/thread/mutex.hpp>
#include <assert.h>
#include <map>
#include <functional>
#include <fstream>

long long logon( wchar_t username[],
                wchar_t password[],
                wchar_t logname[],
                bool debug,
                bool fake )
{
    std::unique_ptr< rithmic::Session > session (
        fake != 0 ? new rithmic::SessionTest( rithmic::Session::toString( username ),
                                              rithmic::Session::toString( password ),
                                              rithmic::Session::toString( logname ),
                                              debug != 0 )
                  : new rithmic::Session( rithmic::Session::toString( username ),
                                          rithmic::Session::toString( password ),
                                          rithmic::Session::toString( logname ),
                                          debug != 0 )
    );

    if ( session->connect() && session->logon() ) {
        return reinterpret_cast< uint64_t >( session.release() );
    }

    return 0;
}

void logout( long long session )
{
    std::unique_ptr< rithmic::Session > p (
        reinterpret_cast< rithmic::Session * > ( session )
    );

    if ( p ) {
        p->disconnect();
    }
}


double RITHMIC_API getAskPrice( long long session,
                                wchar_t symbol[] )
{
    auto p = reinterpret_cast< rithmic::Session * > ( session );

    if ( p != nullptr && p->isConnected() ) {
        return p->readSymbolPrice( rithmic::Session::toString( symbol ), true );
    }

    return 0.0;
}

double RITHMIC_API getBidPrice( long long session,
                                wchar_t symbol[] )
{
    auto p = reinterpret_cast< rithmic::Session * > ( session );

    if ( p != nullptr && p->isConnected() ) {
        return p->readSymbolPrice( rithmic::Session::toString( symbol ), false );
    }

    return 0.0;
}

#pragma once

#include "rithmic.h"
#include "session.hpp"
#include <array>
#include <boost/thread/thread.hpp>

namespace rithmic
{

class RITHMIC_API SessionTest : public Session
{
public:
    SessionTest(const std::string & username,
                const std::string & password,
                const std::string & logname,
                bool debug );

    static wchar_t * symbol();
    static const std::array< double, 16 > & prices();

private:
    bool connect() override;
    void disconnect() override;
    bool subscribeOnSymbol( const std::string & symbol ) override;
    void runTests();

private:
    std::unique_ptr< boost::thread > async_;
};

} // namespace rithmic


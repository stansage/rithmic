#include "rithmic.h"
#include "session_test.hpp"
#include <boost/foreach.hpp>
#include <boost/exception/to_string.hpp>
#include <cassert>
#include <algorithm>
#include <iostream>
#include <memory>
#include <fstream>
#include <set>

int main( int argc, char * argv[] )
{
    wchar_t username[] = L"*========username=========*";
    wchar_t password[] = L"*========password=========*";
    wchar_t logname[] = L"rithmic.log";

    try {
        const auto fake = argc == 2 && *argv[ 1 ] == 'f';
        if ( !fake && argc == 3 ) {
            const auto convert = []( char * src, wchar_t * dst ) {
                auto cur = src;
                do {
                    dst[ cur - src ] = *cur;
                } while ( *cur++ != 0 );
            };
            convert( argv[ 1 ], username );
            convert( argv[ 2 ], password );
        }

        const auto session = logon( username, password, logname, true, fake );
        if ( session == 0 ) {
            throw std::runtime_error( "can't logon" );
        }

        try {
            std::clog << "Testing C interface" << std::endl;
            for ( auto i = 0; i < 10; ++i ) {
                boost::this_thread::sleep_for( boost::chrono::nanoseconds( 1000000000ull ) );
            }
            if ( !fake ) {
                for ( auto i = 1000; i != 0; --i ) {
                    const auto ask = getAskPrice( session, rithmic::SessionTest::symbol() );
                    const auto bid = getBidPrice( session, rithmic::SessionTest::symbol() );

                    if ( !( ask > 0.0 ) ) {
                        throw std::runtime_error( boost::to_string( ask ) );
                    } else if ( !( bid ) ) {
                        throw std::runtime_error( boost::to_string( bid ) );
                    }
                    if ( ask < bid ) {
                        throw std::runtime_error( boost::to_string( ask ) + " < " + boost::to_string( bid ) );
                    }

                    std::clog << "Test passed with ask " << ask << " and bid " << bid << std::endl;

                    boost::this_thread::sleep_for( boost::chrono::nanoseconds( 10000000 ) );
                }
            } else {
                std::set< double > prices[ 2 ];
                for ( auto i = 0u; i < 10000; ++i ) {
                    const auto price = i % 2 == 0
                            ? getBidPrice( session, rithmic::SessionTest::symbol() )
                            : getAskPrice( session, rithmic::SessionTest::symbol() );

                    prices[ i % 2 ].insert( price );

                    boost::this_thread::sleep_for( boost::chrono::nanoseconds( 1000000 ) );
                }

                auto counter = 0;
                auto bid = prices[ 0 ].begin();
                auto ask = prices[ 1 ].begin();

                BOOST_FOREACH( const double & price, rithmic::SessionTest::prices() ) {
                    if ( price - int( price ) > 0.201 ) {
                        continue;
                    }
                    const auto pattern = counter % 2 == 0 ? *bid : *ask;
                    if ( std::fabs( price - pattern ) > 0.001 ) {
                        throw std::runtime_error( boost::to_string( price ) + " != " +
                                                  boost::to_string( pattern ) );
                    }

                    std::clog << "Test passed for price " << price << std::endl;

                    if ( ++counter % 2 == 1 ) {
                        ++bid;
                    } else {
                        ++ask;
                    }
                }
            }
        } catch ( ... ) {
            logout( session );
            throw;
        }
        logout( session );

    } catch ( std::exception & e ) {
        std::cerr << "Test failed: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}

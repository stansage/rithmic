#include "session.hpp"
#include "session_impl.hpp"
#include <boost/foreach.hpp>
#include <boost/thread/thread.hpp>
#include <assert.h>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <sstream>

#define LOG( x ) \
    log() \
        << __FUNCTION__ \
        << x \
        << '}' << std::endl
#define ASSERT_LOG_RESULT( x, r ) \
    assert( x ); \
    if ( ! (x) ) { \
        LOG( " Assert{check:" #x ); \
        return (r); \
    }
#define ASSERT_LOG( x ) \
    assert( x ); \
    if ( ! (x) ) { \
        LOG( " Assert{check:" #x ); \
        return; \
    }
#define ASSERT_LOG_RET( x, r ) \
    assert( x ); \
    if ( ! (x) ) { \
        LOG( " Assert{check:" #x ); \
        return r; \
    }


rithmic::Session::Session( const std::string & username,
                  const std::string & password,
                  const std::string & logname,
                  const bool debug ) :
    impl_( new Impl() )
{
	impl_->engine = nullptr;
	impl_->session = nullptr;
	impl_->isOpen = false;
	impl_->isLogged = false;

    impl_->session = this;
    impl_->username = username;
    impl_->password = password;
    impl_->debug = debug;
    impl_->logPath = logname.empty() ? logname : logname + ".rithmic.log";

    if ( debug || ! logname.empty() ) {
        impl_->log.open( logname );
    }

    if ( impl_->debug ) {
        LOG( " Info{username:" << username << ",workDir:" << Impl::getCwd() );
    }
}

rithmic::Session::~Session()
{
    assert( impl_ != nullptr );

    delete impl_->engine;

    if ( impl_->log.is_open() ) {
        if ( impl_->debug ) {
            LOG( " Info{~" );
        }
        impl_->log.close();
    }

    impl_->engine = nullptr;
}


std::ostream & rithmic::Session::log()
{
    if ( impl_->log.is_open() ) {
        typedef boost::chrono::system_clock Clock;

        auto time = Clock::to_time_t ( Clock::now() );
        return impl_->log << std::ctime( & time ) << " - ";
    }
    return impl_->log;
}

bool rithmic::Session::isConnected() const
{
    boost::lock_guard< boost::mutex > lock( impl_->mutex );

    UNUSED( lock );
    return ( impl_->isOpen &&
           impl_->isLogged /*&&
           impl_->market != Impl::InvalidConnection*/ );
}

bool rithmic::Session::connect()
{
    ASSERT_LOG_RESULT( impl_ != nullptr, false );

    if ( isConnected()  ) {
        return false;
    }

    const char * envp[] = {
        "MML_DMN_SRVR_ADDR=rituz00100.00.rithmic.com:65000~rituz00100.00.rithmic.net:65000~rituz00100.00.theomne.net:65000~rituz00100.00.theomne.com:65000",
        "MML_DOMAIN_NAME=rithmic_uat_dmz_domain",
        "MML_LIC_SRVR_ADDR=rituz00100.00.rithmic.com:56000~rituz00100.00.rithmic.net:56000~rituz00100.00.theomne.net:56000~rituz00100.00.theomne.com:56000",
        "MML_LOC_BROK_ADDR=rituz00100.00.rithmic.com:64100",
        "MML_LOGGER_ADDR=rituz00100.00.rithmic.com:45454~rituz00100.00.rithmic.net:45454~rituz00100.00.theomne.net:45454~rituz00100.00.theomne.com:45454",
        
//        "MML_DMN_SRVR_ADDR=rituz00100.00.rithmic.com:65000~rituz00100.00.rithmic.net:65000~rituz00100.00.theomne.net:65000~rituz00100.00.theomne.com:65000",
//        "MML_DOMAIN_NAME=rithmic_uat_dmz_domain",
//        "MML_LIC_SRVR_ADDR=rituz00100.00.rithmic.com:56000~rituz00100.00.rithmic.net:56000~rituz00100.00.theomne.net:56000~rituz00100.00.theomne.com:56000",
//        "MML_LOC_BROK_ADDR=rituz00100.00.rithmic.com:64100",
//        "MML_LOGGER_ADDR=rituz00100.00.rithmic.com:45454~rituz00100.00.rithmic.net:45454~rituz00100.00.theomne.net:45454~rituz00100.00.theomne.com:45454",


//        "MML_DMN_SRVR_ADDR=ritpa11120.11.rithmic.com:65000~ritpa11120.11.rithmic.net:65000~ritpa11120.11.theomne.net:65000~ritpa11120.11.theomne.com:65000",
//        "MML_DOMAIN_NAME=rithmic_paper_prod_domain",
//        "MML_LIC_SRVR_ADDR=ritpa11120.11.rithmic.com:56000~ritpa11120.11.rithmic.net:56000~ritpa11120.11.theomne.net:56000~ritpa11120.11.theomne.com:56000",
//        "MML_LOC_BROK_ADDR=ritpa11120.11.rithmic.com:64100",
//        "MML_LOGGER_ADDR=ritpa11120.11.rithmic.com:45454~ritpa11120.11.rithmic.net:45454~ritpa11120.11.theomne.net:45454~ritpa11120.11.theomne.com:45454",

//        "MML_DMN_SRVR_ADDR=rituz00100.00.rithmic.com:65000~rituz00100.00.rithmic.net:65000~rituz00100.00.theomne.net:65000~rituz00100.00.theomne.com:65000"
//        "MML_DOMAIN_NAME=rithmic_uat_dmz_domain"
//        "MML_LIC_SRVR_ADDR=rituz00100.00.rithmic.com:56000~rituz00100.00.rithmic.net:56000~rituz00100.00.theomne.net:56000~rituz00100.00.theomne.com:56000"
//        "MML_LOC_BROK_ADDR=rituz00100.00.rithmic.com:64100"
//        "MML_LOGGER_ADDR=rituz00100.00.rithmic.com:45454~rituz00100.00.rithmic.net:45454~rituz00100.00.theomne.com:45454~rituz00100.00.theomne.net:45454"

        "MML_LOG_TYPE=log_net",
        "MML_SSL_CLNT_AUTH_FILE=rithmic_ssl_cert_auth_params",
        "MML_SSL_CLNT_CERT_FILE=rithmic_ssl_client_params",
        "MML_SSL_CLNT_KEY_FILE=rithmic_ssl_client_private_key",
        "USER=koreshkov.e@gmail.com",
        "RAPI_DC_OR_ENCODING=5",
        "RAPI_DC_MD_ENCODING=5",
        nullptr
    };

    RApi::REngineParams engine;
    engine.sAppName = Impl::toNCharcb( "ek:rithmic-mql" );
    engine.sAppVersion = Impl::toNCharcb( "1.0.0.0" );
    engine.sAdmCnnctPt = Impl::toNCharcb( "dd_admin_sslc" );
    engine.sLogFilePath = Impl::toNCharcb( impl_->logPath );
    engine.pAdmCallbacks = impl_.get();
    engine.envp = const_cast< char** >( envp );
//    engine.sDmnSrvrAddr = Impl::toNCharcb( "rituz00100.00.rithmic.com:65000~rituz00100.00.rithmic.net:65000~rituz00100.00.theomne.net:65000~rituz00100.00.theomne.com:65000" );
//    engine.sDomainName = Impl::toNCharcb( "rithmic_uat_dmz_domain" );
//    engine.sLicSrvrAddr = Impl::toNCharcb( "rituz00100.00.rithmic.com:56000~rituz00100.00.rithmic.net:56000~rituz00100.00.theomne.net:56000~rituz00100.00.theomne.com:56000" );
//    engine.sLocBrokAddr = Impl::toNCharcb( "rituz00100.00.rithmic.com:64100" );
//    engine.sLoggerAddr = Impl::toNCharcb( "rituz00100.00.rithmic.com:45454~rituz00100.00.rithmic.net:45454~rituz00100.00.theomne.net:45454~rituz00100.00.theomne.com:45454") ;
//    engine.sCertFile = Impl::toNCharcb( "" );

    try {
        impl_->engine = new RApi::REngine( & engine );
    } catch ( OmneException & exception ) {
        LOG( " Error{code:" << exception.getErrorCode() << ",message:" << exception.getErrorString() );
        return false;
    }

    RApi::LoginParams login;
    login.pCallbacks = impl_.get();
    login.sMdUser = Impl::toNCharcb( impl_->username );
    login.sMdPassword = Impl::toNCharcb( impl_->password );
    login.sMdCnnctPt = Impl::toNCharcb( "login_agent_tpc" );
//    login.sMdCnnctPt = Impl::toNCharcb( "login_agent_tpc" );
    login.sUser = Impl::toNCharcb( impl_->username );
    login.sPassword = Impl::toNCharcb( impl_->password );
    login.sTsCnnctPt = Impl::toNCharcb( "login_agent_opc" );
    login.sIhCnnctPt = Impl::toNCharcb( "login_agent_historyc" );
    login.sPnlCnnctPt = Impl::toNCharcb( "login_agent_pnlc" );
    login.sDcOrCnnctPt = Impl::toNCharcb( "dc_or_Koreshkov_sslc" );
    login.sDcMdCnnctPt = Impl::toNCharcb( "dc_md_Koreshkov_sslc" );

//    * LoginParams.sMdCnnctPt  : login_agent_tpc
//    * LoginParams.sIhCnnctPt  : login_agent_historyc
//    * LoginParams.sTsCnnctPt  : login_agent_opc
//    * LoginParams.sPnLCnnctPt : login_agent_pnlc

    int code = 0;
    if ( ! impl_->engine->login( & login, & code ) ) {
        LOG( " Error{code:" << code << ",username:" << impl_->username << ",password:" << impl_->password[ 0 ] );
        return false;
    }

    return true;
}

void rithmic::Session::disconnect()
{
    ASSERT_LOG( impl_ != nullptr );

    if ( isConnected() ) {
        auto error = 0;

        if ( impl_->debug ) {
            LOG( " Info{open:" << impl_->isOpen << ",logged:" << impl_->isLogged );
        }

		typedef std::pair< std::string, Entry > Pair;
		BOOST_FOREACH( const Pair & pair, impl_->entries ) {
            auto tool = Impl::Tool::fromString( pair.first );
            if ( ! impl_->engine->unsubscribe( & tool.exchange, & tool.ticker, & error ) ) {
                LOG( " WARNING{code:" << error );
            }
        }

        if ( true ) {
            boost::lock_guard< boost::mutex > lock( impl_->mutex );
            UNUSED( lock );
            impl_->entries.clear();
            impl_->isLogged = false;
        }

        if ( ! impl_->engine->logout( & error ) ) {
            LOG( " WARNING{code:" << error );
        }

        ASSERT_LOG( impl_->isOpen == false );
        ASSERT_LOG( impl_->isLogged == false );
    }
}

void rithmic::Session::reconnect( int connection )
{
    ASSERT_LOG( impl_ != nullptr );

    while ( ! isConnected() ) {
        auto error = 0;

        if ( ! impl_->engine->reconnect( connection, &error ) ) {
            LOG( " Error{code:" << error );
        }

        boost::this_thread::sleep_for( boost::chrono::seconds( 2 ) );
    }
}

bool rithmic::Session::logon()
{
    if ( true ) {
        boost::unique_lock< boost::mutex > lock( impl_->mutex );

        if ( ! impl_->isOpen ) {
            impl_->onReady.wait_for( lock, boost::chrono::minutes( 1 ) );
        }
    }
    return isConnected();
}

void rithmic::Session::listExchanges()
{
    auto error = 0;
    std::vector<std::string> result;

    LOG( "Info{listExhanges}" );

    if ( impl_->engine->listExchanges( this, & error ) == 0 ) {
        LOG( " Warn{code:" << error );
    }
}

double rithmic::Session::readSymbolPrice( const std::string & symbol,
                                          const bool ask )
{
    ASSERT_LOG_RET( !symbol.empty(), 0.0 );
    ASSERT_LOG_RET( impl_ != nullptr, 0.0 );
    ASSERT_LOG_RET( isConnected(), 0.0 );

    Entry * entry = nullptr;
    if ( true ) {
        boost::lock_guard< boost::mutex > lock( impl_->mutex );
        UNUSED( lock );

        auto iterator = impl_->entries.find( symbol );

        if ( iterator != impl_->entries.end() ) {
            entry = &iterator->second;
        } else if ( subscribeOnSymbol( symbol ) ) {
            entry = &impl_->entries[ symbol ];
        } else {
            return 0.0;
        }
    }
    ASSERT_LOG_RET( entry != nullptr, 0.0 );

    if ( impl_->debug ) {
        LOG( " Info{ask:" << ask << ",price:" << entry->readPrice( ask ) );
    }

    for ( auto timeout = 1000; ! entry->isReady(); timeout *= 10 ) {
        if ( timeout < 0 ) {
            timeout = 1000;
        }
        boost::this_thread::sleep_for( boost::chrono::nanoseconds( timeout ) );
    }

    boost::lock_guard< boost::mutex > lock( impl_->mutex );
    UNUSED( lock );
    return entry->readPrice( ask );
}

std::string rithmic::Session::toString( const wchar_t * p )
{
    std::wstring ws( p != nullptr ? p : L"" );
    return std::string( ws.begin(), ws.end() );
}

bool rithmic::Session::subscribeOnSymbol( const std::string & symbol )
{
    auto tool = Impl::Tool::fromString( symbol );
    if ( ! Impl::Tool::isValid( tool ) ) {
        LOG( " Warn{symbol:" << symbol );
        return false;
    }

    auto error = 0;
    if ( ! impl_->engine->subscribe( & tool.exchange, & tool.ticker, RApi::MD_QUOTES | RApi::MD_BEST, & error ) ) {
        LOG( " Warn{symbol:" << symbol << ",code:" << error );
        return false;
    }

    return true;
}

//void Session::activateMarketData( const std::string & symbol )
//{
//    ASSERT_LOG( !symbol.empty() );
//    ASSERT_LOG( impl_ != nullptr );
//    ASSERT_LOG( isConnected() );

//    auto tool = Impl::Tool::fromString( symbol );

//    if ( ! Impl::Tool::isValid( tool ) ) {
//        LOG( " Warn{symbol:" << symbol );
//    } else {
//        const auto iterator = impl_->data.find( symbol );
//        if ( iterator == impl_->data.end() ) {
//            auto error = 0;

//            if ( impl_->debug ) {
//                LOG( " Info{symbol:" << symbol );
//            }

//            impl_->data.insert( iterator, std::make_pair( symbol, Entry() ) );
//            if ( ! impl_->engine->subscribe( & tool.exchange, & tool.ticker, RApi::MD_QUOTES | RApi::MD_BEST, & error ) ) {
//                LOG( " Error{symbol:" << symbol <<
//                     ",code:" << error );
//            }
//        }
//    }
//}

void rithmic::Session::onLogon( const int code )
{
    boost::lock_guard< boost::mutex > lock( impl_->mutex );
    UNUSED( lock );

    if ( code != 0 ) {
        if ( ! impl_->isOpen ) {
            impl_->onReady.notify_one();
        } else {
            impl_->isOpen = false;
        }
    } else {
        if ( impl_->isOpen ) {
            impl_->isLogged = true;
            impl_->onReady.notify_one();
        } else {
            impl_->isOpen = true;
        }
    }
}

//void Session::accountReady()
//{
//    impl_->state |= Impl::StateReady;
//}

void rithmic::Session::writeSymbolPrice( const std::string & symbol,
                                         const int type,
                                         const double price )
{
    boost::lock_guard< boost::mutex > lock( impl_->mutex );

    UNUSED( lock );
    const auto iterator = impl_->entries.find( symbol );
    ASSERT_LOG( iterator != impl_->entries.end() );

    switch ( type ) {
    case Impl::BestAsk:
    case Impl::BestBid:
        if ( ! iterator->second.isReady() ) {
            const auto ask = type == Impl::BestAsk;
            iterator->second.writePrice( ask, price );
            iterator->second.commit( ask );
        }
        break;
    case Impl::QuoteAsk:
    case Impl::QuoteBid:
        iterator->second.writePrice( type == Impl::QuoteAsk, price );
        if ( ! iterator->second.isReady() ) {
            iterator->second.commit( type == Impl::QuoteAsk );
        }
        break;
    }

    if ( impl_->debug ) {
        LOG( " Info{type:" << type << ",price:" << price << ",ready:" << iterator->second.isReady() );
    }
}


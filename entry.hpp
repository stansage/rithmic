#pragma once

#include <boost/thread/mutex.hpp>

namespace rithmic
{

class Entry
{
public:
	Entry();
//    void setup( boost::mutex * mutex );
//    std::string toString();
    bool isReady() const;
    double readPrice( const bool ask );
    void writePrice( const bool ask,
                     const double value );
    void commit( const bool ask );
private:
	struct Impl;
	std::shared_ptr< Impl > impl_;
};

} // namespace rithmic


#include "session_impl.hpp"
#include <cassert>
#include <cstring>
#include <algorithm>
#include <boost/thread/thread.hpp>

#define DUMP_ALERT( x ) if ( debug ) {\
    int ignored;\
    session->log() << __FUNCTION__ << '(' << __LINE__ << ')';\
    if ( ! ( x )->dump( & ignored ) ) {\
        session->log() << ':' << __LINE__ << " AlertInfo::dump " << ignored << std::endl;\
    }\
}


#ifdef WIN32
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
 #endif


using namespace rithmic;

bool rithmic::Session::Impl::Tool::operator !() const
{
    return ! isValid( *this );
}

std::string Session::Impl::Tool::toString( const Tool & tool )
{
    return toString( tool.exchange, tool.ticker );
}

std::string Session::Impl::Tool::toString( const tsNCharcb & exchange, const tsNCharcb & ticker )
{
    return Impl::fromNCharcb( ticker ) + '.' + Impl::fromNCharcb( exchange );
}

Session::Impl::Tool Session::Impl::Tool::fromString( const std::string & string )
{
    Tool result;
    const auto position = string.find( '.' );

    if ( position != std::string::npos ) {
        result.symbol[ Ticker ] = string.substr( 0, position );
        result.symbol[ Exchange ] = string.substr( position + 1 );

        result.ticker = Impl::toNCharcb( result.symbol[ Ticker ] );
        result.exchange = Impl::toNCharcb( result.symbol[ Exchange ] );
    }

    return result;
}

bool Session::Impl::Tool::isValid( const Tool & tool )
{
    return ! tool.symbol[ 0 ].empty() &&
           ! tool.symbol[ 1 ].empty() &&
           tool.exchange.iDataLen > 0 &&
           tool.ticker.iDataLen > 0;
}

std::string Session::Impl::getCwd()
{
    char currentPath[ FILENAME_MAX ];

    if ( GetCurrentDir( currentPath, FILENAME_MAX ) ) {
        currentPath[ FILENAME_MAX - 1 ] = '\0';
        return currentPath;
     }

    return std::string();
}

std::string Session::Impl::getUser()
{
    if ( const auto user = std::getenv( "USER" ) ) {
        return user;
    }

    if ( const auto userprofile = std::getenv( "USERPROFILE" ) ) {
        std::string user( userprofile );
        return user.substr( user.rfind( '\\' ) + 1 );
    }

    return std::string();
}

tsNCharcb Session::Impl::toNCharcb( const char * s )
{
    tsNCharcb result;
    if ( ! s || ! s[0] ) {
        result.pData    = nullptr;
        result.iDataLen = 0;
    } else {
        result.pData    = const_cast< char * >( s );
        result.iDataLen = std::strlen( s );
    }

    return result;
}

tsNCharcb Session::Impl::toNCharcb( const std::string & s )
{
    return toNCharcb( s.empty() ? nullptr : s.c_str() );
}

std::string Session::Impl::fromNCharcb( const tsNCharcb & s )
{
    return std::string( s.pData, s.iDataLen );
}

bool Session::Impl::isNCharcbEqual( const tsNCharcb & s1, const tsNCharcb & s2 )
{
    return ( s1.iDataLen == s2.iDataLen ) && ( std::memcmp( s1.pData, s2.pData, s1.iDataLen ) == 0 );
}

boost::uint64_t Session::Impl::toNanoTime( int seconds, int nanoseconds )
{
    auto result = boost::nano::den;

    result *= seconds;
    result += nanoseconds;

    return result;
}

boost::uint64_t Session::Impl::toMicroTime(int seconds, int microseconds)
{
    auto result = boost::micro::den;

    result *= seconds;
    result += microseconds;

    return result;
}

Session::Impl::Tool Session::Impl::makeTool( const std::string & symbol )
{
    return Tool::fromString( symbol );
}


int Session::Impl::Alert( RApi::AlertInfo * info, void * context, int * code )
{
    UNUSED( context );

    if ( debug ) {
        session->log()
                << __FUNCTION__
                << " Info{connection:" << info->iConnectionId
                << ",type:" << info->iAlertType
                << ",code:" << info->iRpCode
                << '}' << std::endl;
    }

    switch ( info->iAlertType ) {
    case RApi::ALERT_CONNECTION_OPENED:
        if ( info->iConnectionId == RApi::MARKET_DATA_CONNECTION_ID ) {
            session->onLogon( ConnectionOpened );
        }
        break;

    case RApi::ALERT_LOGIN_COMPLETE:
        if ( info->iConnectionId == RApi::MARKET_DATA_CONNECTION_ID ) {
            session->onLogon( ConnectionReady );
        }
        break;

    case RApi::ALERT_CONNECTION_BROKEN:
        session->onLogon( ConnectionBroken );
        break;

    case RApi::ALERT_CONNECTION_CLOSED:
        session->onLogon( ConnectionClosed );
        if ( session->impl_->isLogged ) {
            session->reconnect( info->iConnectionId );
        }
        break;

    case RApi::ALERT_FORCED_LOGOUT:
        session->disconnect();
        break;

    case RApi::ALERT_SERVICE_ERROR:
        if ( info->iRpCode == 7 ) {
            entries.erase( Tool::toString( info->sExchange, info->sTicker ) );
            break;
        }

    default:
        DUMP_ALERT( info )
        break;
    }

    * code = API_OK;
    return OK;
}


int Session::Impl::AccountList( RApi::AccountListInfo * info, void * context, int * code )
{
    UNUSED( context );
//    DUMP_ALERT( info )

    if ( info->iArrayLen > 0 ) {
        const auto & accountInfo = info->asAccountInfoArray[ 0 ];
        account.id = fromNCharcb( accountInfo.sAccountId );
        account.ibId = fromNCharcb( accountInfo.sIbId );
        account.fcmId = fromNCharcb( accountInfo.sFcmId );
        account.info.sIbId = toNCharcb( account.ibId );
        account.info.sFcmId = toNCharcb( account.fcmId );
        account.info.sAccountId = toNCharcb( account.id );
        if ( debug ) {
               session->log() << __FUNCTION__
                              << " Info{id:" << account.id
                              << ",ibId:" << account.ibId
                              << ",fcmId:" << account.fcmId
                              << '}' << std::endl;

        }
        session->listExchanges();
    }

    * code = API_OK;
    return OK;
}

int Session::Impl::AskQuote( RApi::AskInfo * info, void * context, int * code )
{
    UNUSED( context );
//    DUMP_ALERT( info )

    if ( debug ) {
        session->log()
                << __FUNCTION__
                << " Info{type:" << info->iType
                << ",hasPrice:" << info->bPriceFlag
                << ",hasSize:" << info->bSizeFlag
                << ",price:" << info->dPrice
                << ",size:" << info->iSize
                << '}' << std::endl;
    }

    if ( info->bPriceFlag ) {
        const auto symbol = Impl::Tool::toString( info->sExchange, info->sTicker );
        session->writeSymbolPrice( symbol, QuoteAsk, info->dPrice );
    }

    * code = API_OK;
    return OK;
}

int Session::Impl::BestAskQuote( RApi::AskInfo * info, void * context, int * code )
{
    UNUSED( context );
//    DUMP_ALERT( info );

    if ( debug ) {
        session->log()
                << __FUNCTION__
                << " Info{type:" << info->iType
                << ",hasPrice:" << info->bPriceFlag
                << ",hasSize:" << info->bSizeFlag
                << ",price:" << info->dPrice
                << ",size:" << info->iSize
                << '}' << std::endl;
    }

    if ( info->bPriceFlag ) {
        const auto symbol = Impl::Tool::toString( info->sExchange, info->sTicker );
        session->writeSymbolPrice( symbol, BestAsk, info->dPrice );
    }

    * code = API_OK;
    return OK;
}

int Session::Impl::BestBidAskQuote( RApi::BidInfo * infoBid, RApi::AskInfo * infoAsk, void * context, int * code )
{
    UNUSED( context );
//    DUMP_ALERT( infoBid );
//    DUMP_ALERT( infoAsk );

    if ( debug ) {
        session->log()
                << __FUNCTION__
                << " Info{bidType:" << infoBid->iType
                << ",bidHasPrice:" << infoBid->bPriceFlag
                << ",bidHasSize:" << infoBid->bSizeFlag
                << ",bidPrice:" << infoBid->dPrice
                << ",bidSize:" << infoBid->iSize
                << ",askType:" << infoAsk->iType
                << ",askHasPrice:" << infoAsk->bPriceFlag
                << ",askHasSize:" << infoAsk->bSizeFlag
                << ",askPrice:" << infoAsk->dPrice
                << ",askSize:" << infoAsk->iSize
                << '}' << std::endl;
    }

    if ( infoBid->bPriceFlag ) {
        const auto symbol = Impl::Tool::toString( infoBid->sExchange, infoBid->sTicker );
        session->writeSymbolPrice( symbol, BestBid, infoBid->dPrice );
    }

    if ( infoAsk->bPriceFlag ) {
        const auto symbol = Impl::Tool::toString( infoAsk->sExchange, infoAsk->sTicker );
        session->writeSymbolPrice( symbol, BestAsk, infoAsk->dPrice );
    }

    * code = API_OK;
    return OK;
}

int Session::Impl::BestBidQuote( RApi::BidInfo * info, void * context, int * code )
{
    UNUSED( context );

    if ( debug ) {
        session->log()
                << __FUNCTION__
                << " Info{type:" << info->iType
                << ",hasPrice:" << info->bPriceFlag
                << ",hasSize:" << info->bSizeFlag
                << ",price:" << info->dPrice
                << ",size:" << info->iSize
                << '}' << std::endl;
    }

    if ( info->bPriceFlag ) {
        const auto symbol = Impl::Tool::toString( info->sExchange, info->sTicker );
        session->writeSymbolPrice( symbol, BestBid, info->dPrice );
    }


    * code = API_OK;
    return OK;
}

int Session::Impl::BidQuote( RApi::BidInfo * info, void * context, int * code )
{
    UNUSED( context );
//    DUMP_ALERT( info );

    if ( debug ) {
        session->log()
                << __FUNCTION__
                << " Info{type:" << info->iType
                << ",hasPrice:" << info->bPriceFlag
                << ",hasSize:" << info->bSizeFlag
                << ",price:" << info->dPrice
                << ",size:" << info->iSize
                << '}' << std::endl;
    }

    if ( info->bPriceFlag ) {
        const auto symbol = Impl::Tool::toString( info->sExchange, info->sTicker );
        session->writeSymbolPrice( symbol, QuoteBid, info->dPrice );
    }

    * code = API_OK;
    return OK;
}

int Session::Impl::BinaryContractList( RApi::BinaryContractListInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::BustReport( RApi::OrderBustReport * report, void * context, int * code )
{
    UNUSED( report );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::CancelReport( RApi::OrderCancelReport * report, void * context, int * code )
{
    UNUSED( report );
    UNUSED( context );

    * code = API_OK;
    return OK;
}


int Session::Impl::ClosePrice( RApi::ClosePriceInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::ClosingIndicator( RApi::ClosingIndicatorInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::EndQuote( RApi::EndQuoteInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::EquityOptionStrategyList( RApi::EquityOptionStrategyListInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::ExchangeList( RApi::ExchangeListInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    if ( debug ) {
        session->log() << __FUNCTION__
                       << " Info{exchangeiListSize:" << info->iArrayLen
                       << '}' << std::endl;
    }
    * code = API_OK;
    return OK;
}

int Session::Impl::ExecutionReplay( RApi::ExecutionReplayInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::FailureReport( RApi::OrderFailureReport * report, void * context, int * code )
{
    UNUSED( report );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::FillReport( RApi::OrderFillReport * report, void * context, int * code )
{
    UNUSED( report );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::HighPrice( RApi::HighPriceInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::InstrumentByUnderlying( RApi::InstrumentByUnderlyingInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::LimitOrderBook( RApi::LimitOrderBookInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::LineUpdate( RApi::LineInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::LowPrice( RApi::LowPriceInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::MarketMode( RApi::MarketModeInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}


int Session::Impl::ModifyReport( RApi::OrderModifyReport * report, void * context, int * code )
{
    UNUSED( report );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::NotCancelledReport( RApi::OrderNotCancelledReport * report, void * context, int * code )
{
    UNUSED( report );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::NotModifiedReport( RApi::OrderNotModifiedReport * report, void * context, int * code )
{
    UNUSED( report );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::OpenInterest( RApi::OpenInterestInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::OpenOrderReplay( RApi::OrderReplayInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::OpenPrice( RApi::OpenPriceInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::OpeningIndicator( RApi::OpeningIndicatorInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::OptionList( RApi::OptionListInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}


int Session::Impl::OrderReplay( RApi::OrderReplayInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::OtherReport( RApi::OrderReport * report, void * context, int * code )
{
    UNUSED( report );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::PasswordChange( RApi::PasswordChangeInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

    * code = API_OK;
    return OK;
}

int Session::Impl::PnlReplay( RApi::PnlReplayInfo * info, void * context, int * code )
{
    UNUSED( context );
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::PnlUpdate( RApi::PnlInfo * info, void * context, int * code )
{
    UNUSED( context );
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::PriceIncrUpdate( RApi::PriceIncrInfo * info, void * context, int * code )
{
    UNUSED( context );
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::ProductRmsList( RApi::ProductRmsListInfo * info, void * context, int * code )
{
    UNUSED( context );
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::RefData( RApi::RefDataInfo * info, void * context, int * code )
{
    UNUSED( context );
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::RejectReport( RApi::OrderRejectReport * report, void * context, int * code )
{
    UNUSED( context );
    //DUMP_ALERT( report )

    * code = API_OK;
    return OK;
}

int Session::Impl::SettlementPrice( RApi::SettlementPriceInfo * info, void * context, int * code )
{
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::SingleOrderReplay( RApi::SingleOrderReplayInfo * info, void * context, int * code )
{
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::SodUpdate( RApi::SodReport * report, void * context, int * code )
{
    DUMP_ALERT( report )

    * code = API_OK;
    return OK;
}

int Session::Impl::StatusReport( RApi::OrderStatusReport * report, void * context, int * code )
{
    UNUSED( context );
    //DUMP_ALERT( report )

    * code = API_OK;
    return OK;
}

int Session::Impl::Strategy( RApi::StrategyInfo * info, void * context, int * code )
{
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::StrategyList( RApi::StrategyListInfo * info, void * context, int * code )
{
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::TradeReplay( RApi::TradeReplayInfo * info, void * context, int * code )
{
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::TradeRoute( RApi::TradeRouteInfo * info, void * context, int * code )
{
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}


int Session::Impl::TradeRouteList( RApi::TradeRouteListInfo * info, void * context, int * code )
{
    UNUSED( info );
    UNUSED( context );

//    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::TriggerPulledReport( RApi::OrderTriggerPulledReport * report, void * context, int * code )
{
    DUMP_ALERT( report );

    * code = API_OK;
    return OK;
}

int Session::Impl::TriggerReport( RApi::OrderTriggerReport * report, void * context, int * code )
{
    DUMP_ALERT( report );

    * code = API_OK;
    return OK;
}

int Session::Impl::TradeCondition( RApi::TradeInfo * info, void * context, int * code )
{
    DUMP_ALERT( info );

    * code = API_OK;
    return OK;
}

int Session::Impl::TradeCorrectReport( RApi::OrderTradeCorrectReport * report, void * context, int * code )
{
    DUMP_ALERT( report );

    * code = API_OK;
    return OK;
}

int Session::Impl::TradePrint( RApi::TradeInfo * info, void * context, int * code )
{
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}

int Session::Impl::TradeVolume( RApi::TradeVolumeInfo * info, void * context, int * code )
{
    DUMP_ALERT( info )

    * code = API_OK;
    return OK;
}



#include "session_test.hpp"
#include "session_impl.hpp"
#include <boost/foreach.hpp>
#include <boost/exception/to_string.hpp>

rithmic::SessionTest::SessionTest( const std::string & username,
                                   const std::string & password,
                                   const std::string & logname,
                                   bool debug ) :
    Session( username, password, logname, debug )
{
}

wchar_t * rithmic::SessionTest::symbol()
{
    static wchar_t result[] = L"6EN7.CME";
    return result;
}

const std::array< double, 16 > & rithmic::SessionTest::prices()
{
    static bool empty = true;
    static std::array< double, 16 > result;

    if ( empty ) {
        empty = false;
        result[ 0 ] = 1.1;
        result[ 1 ] = 1.2;
        result[ 2 ] = 1.3;
        result[ 3 ] = 1.4;
        result[ 4 ] = 2.1;
        result[ 5 ] = 2.2;
        result[ 6 ] = 2.3;
        result[ 7 ] = 2.4;
        result[ 8 ] = 3.1;
        result[ 9 ] = 3.2;
        result[ 10 ] = 3.3;
        result[ 11 ] = 3.4;
        result[ 12 ] = 4.1;
        result[ 13 ] = 4.2;
        result[ 14 ] = 4.3;
        result[ 15 ] = 4.4;
    }

	return result;
}

bool rithmic::SessionTest::connect()
{
	async_.reset( new  boost::thread( &rithmic::SessionTest::runTests, this ) );
    return true;
}

void rithmic::SessionTest::disconnect()
{
    if ( isConnected() ) {
        async_->join();
		async_.reset();

        impl_->isOpen = false;
        impl_->isLogged = false;
        impl_->entries.clear();
    }
}

bool rithmic::SessionTest::subscribeOnSymbol( const std::string & symbol )
{
    if ( impl_->debug ) {
        log() << "Subscribing on " << symbol << std::endl;
    }

    return true;
}

void rithmic::SessionTest::runTests()
{
	try {
        if ( impl_->debug ) {
            log() << "Running tests" << std::endl;
        }

        boost::this_thread::sleep_for( boost::chrono::milliseconds( impl_->debug ? 1000 : 100 ) );

        auto code = 0;

        RApi::AlertInfo info;
        info.iAlertType = RApi::ALERT_CONNECTION_OPENED;
        info.iConnectionId = RApi::MARKET_DATA_CONNECTION_ID;

        if ( impl_->Alert( &info, nullptr, &code ) == NOT_OK || code != API_OK ) {
            throw std::runtime_error( "can't open connection" );
        }

        boost::this_thread::sleep_for( boost::chrono::milliseconds( impl_->debug ? 1500 : 150 ) );
        info.iAlertType = RApi::ALERT_LOGIN_COMPLETE;

        if ( impl_->Alert( &info, nullptr, &code ) == NOT_OK || code != API_OK ) {
            throw std::runtime_error( "can't login" );
        }

        auto tool = impl_->makeTool( toString( symbol() ) );
        if ( !tool ) {
            throw std::runtime_error( "invalid symbol " + toString( symbol() ) );
        }

        auto counter = 0;
        BOOST_FOREACH ( const double price, prices() ) {
            boost::this_thread::sleep_for( boost::chrono::milliseconds( 500 ) );

            RApi::BidInfo bidInfo;
            RApi::AskInfo askInfo;

            bidInfo.bPriceFlag = true;
            askInfo.bPriceFlag = true;
            bidInfo.bSizeFlag = true;
            askInfo.bSizeFlag = true;
            bidInfo.iSize = 1;
            askInfo.iSize = 1;
            bidInfo.sTicker = tool.ticker;
            askInfo.sTicker = tool.ticker;
            bidInfo.sExchange = tool.exchange;
            askInfo.sExchange = tool.exchange;
            bidInfo.dPrice = price;
            askInfo.dPrice = price;

            auto best = ( counter >> 1 ) == 1;
            auto ask = ( counter & 1 ) == 1;
            auto ok = false;

            if ( best && ask ) {
                ok = impl_->BestAskQuote( &askInfo, nullptr, &code ) == OK && code == API_OK;
            } else if ( best && !ask ) {
                ok = impl_->BestBidQuote( &bidInfo, nullptr, &code ) == OK && code == API_OK;
            } else if ( !best && ask  ) {
                ok = impl_->AskQuote( &askInfo, nullptr, &code ) == OK && code == API_OK;
            } else if ( !best && !ask  ) {
                ok = impl_->BidQuote( &bidInfo, nullptr, &code ) == OK && code == API_OK;
            }

            if ( !ok ) {
                throw std::runtime_error( "can't quote " + boost::to_string( counter ) );
            }

            if ( ++counter % 4 == 0 ) {
                counter = 0;
            }
        }
	} catch ( const std::exception & e ) {
        if ( impl_->debug ) {
            log() << "Test error: " << e.what() << std::endl;
        }
    }

    if ( impl_->debug ) {
        log() << "Stopping tests" << std::endl;
    }
}

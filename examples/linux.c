#include <rithmic.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    if (argc < 4)
    {
        printf("Usage: %s USER PASSWORD SYMBOL\n", argv[0]);
        return 1;
    }
    size_t l_len = strlen(argv[0]);
    size_t u_len = strlen(argv[1]);
    size_t p_len = strlen(argv[2]);
    size_t s_len = strlen(argv[3]);
    wchar_t* logname  = calloc(l_len + 1, sizeof(wchar_t));
    wchar_t* username = calloc(u_len + 1, sizeof(wchar_t));
    wchar_t* password = calloc(p_len + 1, sizeof(wchar_t));
    wchar_t* symbol   = calloc(s_len + 1, sizeof(wchar_t));

    mbstowcs(logname,  argv[0], l_len);
    mbstowcs(username, argv[1], u_len);
    mbstowcs(password, argv[2], p_len);
    mbstowcs(symbol,   argv[3], s_len);

    long long session = logon(username, password, logname, true, false);

    printf("ASK(%s) = %0.6f\n", argv[3], getAskPrice( session, symbol ) );
    printf("BID(%s) = %0.6f\n", argv[3], getBidPrice( session, symbol ) );

    logout(session);
    free(username);
    free(password);

    return 0;
}

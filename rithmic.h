#pragma once

#if defined (_WIN32)
#if defined(rithmic_EXPORTS)
#define  RITHMIC_API __declspec(dllexport)
#else
#define  RITHMIC_API __declspec(dllimport)
#endif
#else
#define RITHMIC_API
#endif

#ifndef __cplusplus
#include <wchar.h>
#include <stdbool.h>
#endif


#ifdef __cplusplus
extern "C"
{
#endif
    long long RITHMIC_API logon( wchar_t username[],
                                 wchar_t password[],
                                 wchar_t logname[],
                                 bool debug,
                                 bool fake );
    void RITHMIC_API logout( long long session );

    double RITHMIC_API getAskPrice( long long session,
                                    wchar_t symbol[] );
    double RITHMIC_API getBidPrice( long long session,
                                    wchar_t symbol[] );
#ifdef __cplusplus
}
#endif

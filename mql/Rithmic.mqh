//+------------------------------------------------------------------+
//|                                                      Rithmic.mqh |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                                         https://www.stansage.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/rithmic"
#property version   "1.0"
#property strict
#import "rithmic.dll"

long logon( string username, string password, string logname, bool debug, bool test );
void logout( ulong session );
double getAskPrice( long session, string symbol );
double getBidPrice( long session, string symbol );

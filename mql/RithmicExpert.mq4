//+------------------------------------------------------------------+
//|                                                RithmicExpert.mq4 |
//|                       Copyright 2015, Stan Sage me@stansage.com. |
//|                                         https://www.stansage.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Stan Sage me@stansage.com."
#property link      "https://bitbucket.org/stansage/rithmic"
#property version   "1.0"
#property strict
#include <Rithmic.mqh>

long session = 0;

int OnInit()
{
   EventSetTimer( 1 );

   session = logon( "USER", "PASSWORD", "logs/RithmicExpert_" + TimeToString( TimeLocal(), TIME_DATE ) + ".log", true, true );

   return session == 0 ? INIT_FAILED : INIT_SUCCEEDED;
}

void OnDeinit( const int reason )
{
    EventKillTimer();

    logout( session );
}

void OnTimer()
{
    double ask = getAskPrice( session, "6EH6.CME" );
    double bid = getBidPrice( session, "6EH6.CME" );
    Print( "Price: ask: ", ask, ", bid: ", bid );
}

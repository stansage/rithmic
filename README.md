# Build #
Use [CMake](http://www.cmake.org/) to create build files.
The build script requires boost libraries properly installed.

## API ##
```
long long logon( char username[], char password[], char logPath[], bool debug, bool fake );
void logout( long long session );
double getAskPrice( long long sessionId, char symbol[] );
double getBidPrice( long long sessionId, char symbol[] );
```

## Symbol format ##
**TICKER.EXCHANGE**

## MQL integration ##
* Copy ** RithmicExpert.mq4 ** to directory MQL4\Experts
* Copy ** Rithmic.mqh ** to directory MQL4\Include directory
* Copy ** rithmic.dll ** to directory MQL4\Libraries directory
* Copy ** api/etc/rithmic_ssl_* ** to terminal.exe working directory
Use this library as shown in sample expert ** RithmicExpert.mq4 **.

## Linux usage ##
* Get sources from git repository ** git clone https://bitbucket.org/stansage/rithmic.git **
* Build library with CMake ** cd rithmic && cmake -D CMAKE_BUILD_TYPE=Release && make **
* Build sample ** cd examples && gcc -L.. -lrithmic linux.c -o test_rithmic **
* Place certificates to working directory ** cp ../api/etc/rithmic_ssl_* .  **
* Run program with real username, password and symbol ** LD_LIBRARY_PATH=.. ./test_rithmic <USERNAME> <PASSWORD> <SYMBOL> **


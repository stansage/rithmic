#pragma once

#include <string>
#include <memory>
#include <ostream>

#define UNUSED( x ) (void)(x)

namespace rithmic
{

class Session
{
public:
    Session( const std::string & username,
             const std::string & password,
             const std::string & logname,
             const bool debug );
    ~Session();

    std::ostream & log();

    bool isConnected() const;
    virtual bool connect();
    virtual void disconnect();
    void reconnect( int connection );

    bool logon();

    void listExchanges();

    double readSymbolPrice( const std::string & symbol,
                            const bool ask );

    static std::string toString( const wchar_t * p );

protected:
    virtual bool subscribeOnSymbol( const std::string & symbol );
    void onLogon( const int code );
    void writeSymbolPrice( const std::string & symbol,
                           const int type,
                           const double price );
protected:
    struct Impl;
    friend struct Impl;
    std::shared_ptr< Impl > impl_;
};

} // namespace rithmic


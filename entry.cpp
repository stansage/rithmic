#include "entry.hpp"
#include <boost/foreach.hpp>
#include <boost/atomic/atomic.hpp>
#include <boost/exception/to_string.hpp>
#include <boost/thread/lock_guard.hpp>
#include <cassert>
#include <string>
#include <array>

struct rithmic::Entry::Impl
{
    boost::atomic_char state;
    std::array< double, 2 > prices;
};

rithmic::Entry::Entry() :
    impl_( new Impl() )
{
    impl_->state = 0;
	BOOST_FOREACH( double & price, impl_->prices ) {
        price = 0.0;
    }
}

bool rithmic::Entry::isReady() const
{
    return impl_->state == 3;
}

//void rithmic::Entry::setup( boost::mutex * mutex )
//{
//    impl_->mutex = mutex;
//}

//std::string rithmic::Entry::toString()
//{
//    std::string result;

//    BOOST_FOREACH( const double & price, impl_->prices ) {
//        result += boost::to_string( price );
//        result.push_back( ',' );
//    }
//    result.pop_back();

//    return result;
//}

double rithmic::Entry::readPrice( const bool ask )
{
    return impl_->prices[ ask  ];
}

void rithmic::Entry::writePrice( const bool ask,
                                 const double value )
{
    impl_->prices[ ask ] = value;
}

void rithmic::Entry::commit( const bool ask )
{
    impl_->state |= ask + 1;
}

